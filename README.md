# CountOfMoney

This application provides a real-time view of the market price of crypto-currencies. The solution includes details for each crypto-currency such as the latest associated news and a description of the project. 

The price of crypto-currencies is retrieved from the coingecko API. The project is developed in ReactJS for the frontend and in Node.js under the NestJS framework for the backend.
